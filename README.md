# firefox-chrome

chrome directory for firefox to override bad CSS

## References

- [1] http://superuser.com/questions/318912/how-to-override-the-css-of-a-site-in-firefox-with-usercontent-css
- [2] https://ffeathers.wordpress.com/2013/03/10/how-to-override-css-stylesheets-in-firefox/
- [3] http://stackoverflow.com/questions/1461077/how-do-i-remove-background-image-in-css
- [4] http://thebarnharts.com/firefoxcssoverride.asp
